import { Html, Head, Main, NextScript } from 'next/document'

export default function MyTitle(props:any) {
  return (
    <>
    <div style={{textAlign:"center"}}>
        <h1>{props.title}</h1>
        <p>DOCENTE: EL MUNDO ES TAN INCREIBLE QUE NO NOS DAMOS CUENTA</p>
        <p>ESTUDIANTE: LA VIDA ES MARAVILLOSO PARA VIVIR Y CONOCER GENTE </p>
      </div>
    </>
  )
}